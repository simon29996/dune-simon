#include <config.h>
#include <options.h>

#include <string>
#include <random>

#include <dune/common/timer.hh>

#include <dune/geometry/quadraturerules.hh>

#include <dune/grid/yaspgrid.hh>
#include <dune/grid/io/file/vtk/vtksequencewriter.hh>

#include <dune/istl/matrix.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/cholmod.hh>
#include <dune/istl/umfpack.hh>
#include <dune/istl/matrixmarket.hh>

#include <dune/functions/functionspacebases/interpolate.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>

using namespace Dune;


// Generate the index set Z_{eta,>}
void indexSet(const int& eta, int& modenumber, std::vector<int>& k0,
  std::vector<int>& k1)
{
  modenumber = 0;

  for(int i = 0; i <= eta; i++){
    int s;
    if (i == 0){
      s = 1;
    }
    else{
      s = -eta;
    }

    for(int j = s; j <= eta; j++){
      auto norm = std::sqrt(i*i + j*j);

      if (norm <= eta){
        modenumber += 1;
        k0.push_back(i);
        k1.push_back(j);
      }
    }
  }
}


// Generate a random graph function
template <class VectorType>
void generateSurfaceFunction(int& modenumber, std::vector<int>& k0, std::vector<int>& k1,
  VectorType& xk, VectorType& yk, const double& kappa, const double& sigma)
{
  double pi = std::acos(-1.0);

  xk.resize(modenumber);
  yk.resize(modenumber);

  std::random_device rd{};
  std::mt19937 gen{rd()};
  std::normal_distribution<> d;

  for(int i = 0; i < modenumber; i++){
    auto normsq = k0[i]*k0[i] + k1[i]*k1[i];
    double factor = std::sqrt(2*normsq*(4*pi*pi*kappa*normsq + sigma));
    xk[i] = d(gen)/factor;
    yk[i] = d(gen)/factor;
  }
}


// Evaluate the first fundamental form of the surface at a given point x
template <class CoordinateType, class VectorType, class MatrixType>
void getFundamentalMatrix(CoordinateType& x, MatrixType& fundamentalmatrix,
  int& modenumber, std::vector<int>& k0, std::vector<int>& k1, VectorType& xk,
  VectorType& yk, double& pi)
{
  double sp;
  double factor;
  std::vector<double> gradient;

  gradient.resize(2);
  gradient[0] = 0;
  gradient[1] = 0;

  for(int i=0; i < modenumber; i++){
    sp = 2*pi*(x[0]*k0[i] + x[1]*k1[i]);
    factor = std::cos(sp)*xk[i] + std::sin(sp)*yk[i];

    gradient[0] += k0[i] * factor;
    gradient[1] += k1[i] * factor;
  }

  fundamentalmatrix[0][0] = 1 + 4*gradient[0]*gradient[0];
  fundamentalmatrix[0][1] = 4*gradient[0]*gradient[1];
  fundamentalmatrix[1][0] = 4*gradient[1]*gradient[0];
  fundamentalmatrix[1][1] = 1 + 4*gradient[1]*gradient[1];
}


// Assemble the local stiffness and mass matrices
template <class LocalView, class VectorType, class MatrixType>
void getLocalMatrices(const LocalView& localview, MatrixType& localstiffnessmatrix,
  MatrixType& localmassmatrix, int& modenumber, std::vector<int>& k0,
  std::vector<int>& k1, VectorType& xk, VectorType& yk, double& pi)
{
  // Get the grid element from the local FE basis view
  typedef typename LocalView::Element Element;
  const Element& element = localview.element();

  const int dim = Element::dimension;
  auto geometry = element.geometry();

  // Get set of shape functions for this element
  const auto& localFiniteElement = localview.tree().finiteElement();

  // Set all matrix entries to zero
  localstiffnessmatrix.setSize(localFiniteElement.localBasis().size(), localFiniteElement.localBasis().size());
  localstiffnessmatrix = 0;
  localmassmatrix.setSize(localFiniteElement.localBasis().size(), localFiniteElement.localBasis().size());
  localmassmatrix = 0;

  // Get a quadrature rule
  const int quadOrder = 2*localFiniteElement.localBasis().order();
  const QuadratureRule<double, dim>& quad = QuadratureRules<double, dim>::rule(element.type(), quadOrder);

  // Loop over all quadrature points
  for (size_t pt=0; pt < quad.size(); pt++) {

    // Position of the current quadrature point in the reference element
    const FieldVector<double,dim>& quadPos = quad[pt].position();
    // std::cout << quadPos << std::endl;

    // Position of the current quadrature point in the actual element
    const auto globalQuadPos = geometry.global(quadPos);

    // The transposed inverse Jacobian of the map from the reference element to the element
    const auto& jacobian = geometry.jacobianInverseTransposed(quadPos);

    // The multiplicative factor in the integral transformation formula
    const double integrationElement = geometry.integrationElement(quadPos);

    // The gradients of the shape functions on the reference element
    std::vector<FieldMatrix<double,1,dim>> referenceGradients;
    localFiniteElement.localBasis().evaluateJacobian(quadPos, referenceGradients);

    // Compute the shape function gradients on the real element
    std::vector<FieldVector<double,dim>> gradients(referenceGradients.size());
    for (size_t i=0; i<gradients.size(); i++){
      jacobian.mv(referenceGradients[i][0], gradients[i]);
    }

    // The values of the shape functions on the reference element
    std::vector<FieldVector<double,1>> values;
    localFiniteElement.localBasis().evaluateFunction(quadPos, values);

    FieldMatrix<double,dim,dim> fundamentalMatrix;
    getFundamentalMatrix(globalQuadPos, fundamentalMatrix, modenumber, k0, k1, xk, yk, pi);
    auto transformationFactor = std::sqrt(fundamentalMatrix.determinant());
    auto transformationMatrix = fundamentalMatrix;
    transformationMatrix.invert();

    // Compute the actual matrix entries
    FieldVector<double,dim> x;

    for (size_t i=0; i<localstiffnessmatrix.N(); i++){

      transformationMatrix.mv(gradients[i],x);

      for (size_t j=0; j<localstiffnessmatrix.M(); j++ ){
        localstiffnessmatrix[i][j] += transformationFactor * ( x*gradients[j] ) * quad[pt].weight() * integrationElement;
        localmassmatrix[i][j] += transformationFactor * (values[i]*values[j]) * quad[pt].weight() * integrationElement;
      }
    }
  }

}


// Return true/false depending on if the index is on the right or top boundary of the domain
template <class IndexType>
bool isOnBoundary(IndexType& index, const int& elementnumber, const int& feorder)
{
  // Technicality to avoid comparing int with unsigned int
  int Index = index;

  if (Index <= elementnumber*(elementnumber+2)){
    if (Index >= (elementnumber+1)*elementnumber){
      return true;
    }
    else if (Index%(elementnumber+1) == elementnumber){
      return true;
    }
  }
  else if (Index <= elementnumber*((elementnumber+1)*feorder + 1)){
    if (Index >= elementnumber*(elementnumber*feorder + 2) + 1){
      return true;
    }
  }
  else if (Index >= (elementnumber+1)*(elementnumber*feorder + 1) && Index <= elementnumber*((elementnumber+1)*(2*feorder - 1) + 1)){
    if ((Index - (elementnumber+1)*(elementnumber+1))%((elementnumber+1)*(feorder-1)) >= elementnumber*(feorder - 1)){
      return true;
    }
  }
  return false;
}


// Shift index, if it is on the right/top boundary, to the identified one on the left/bottom boundary
template <class IndexType>
void indexCorrection(IndexType& index, const int& elementnumber, const int& feorder)
{
  // Technicality to avoid comparing int with unsigned int
  int Index = index;

  if (Index <= elementnumber*(elementnumber+2)){
    if (Index == elementnumber*(elementnumber+2)){
      Index = 0;
    }
    else if (Index >= (elementnumber+1)*elementnumber){
      Index -= (elementnumber+1)*elementnumber;
    }
    else if (Index%(elementnumber+1) == elementnumber){
      Index -= elementnumber;
    }
  }
  else if (Index <= elementnumber*((elementnumber+1)*feorder + 1)){
    if (Index >= elementnumber*(elementnumber*feorder + 2) + 1){
      Index -= elementnumber*elementnumber*(feorder-1);
    }
  }
  else if (Index >= (elementnumber+1)*(elementnumber*feorder + 1) && Index <= elementnumber*((elementnumber+1)*(2*feorder - 1) + 1)){
    if ((Index - (elementnumber+1)*(elementnumber+1))%((elementnumber+1)*(feorder-1)) >= elementnumber*(feorder - 1)){
      Index -= elementnumber*(feorder-1);
    }
  }

  index = Index;
}


// Get the occupation pattern of the stiffness/mass matrix
template <class FEBasis>
void getOccupationPattern(const FEBasis& febasis, MatrixIndexSet& matrixindexset,
  const int& elementnumber, const int& feorder)
{
  // Total number of grid vertices
  const int n = febasis.size();

  matrixindexset.resize(n, n);

  // A view on the FE basis on a single element
  auto localView = febasis.localView();

  // Loop over all leaf elements
  for(const auto& e : elements(febasis.gridView()))
  {
    // Bind the local FE basis view to the current element
    localView.bind(e);

    // There is a matrix entry a_ij if the i-th and j-th vertex are connected in the grid
    for (size_t i=0; i<localView.tree().size(); i++) {

      auto iIdx = localView.index(i);
      indexCorrection(iIdx[0], elementnumber, feorder);

      for (size_t j=0; j<localView.tree().size(); j++) {

        auto jIdx = localView.index(j);
        indexCorrection(jIdx[0], elementnumber, feorder);

        // Add a nonzero entry to the matrix
        matrixindexset.add(iIdx, jIdx);

      }

    }

  }

  // Add nonzero entries on the diagonal of the boundary nodes
  for(int i = 0; i < n; i++){
    if (isOnBoundary(i, elementnumber, feorder)){
      matrixindexset.add(i,i);
    }
  }

}


// Assemble the Laplace stiffness and mass matrix on the given grid view
template <class FEBasis, class VectorType, class MatrixType>
void assembleMatrices(const FEBasis& febasis, MatrixType& stiffnessmatrix,
  MatrixType& massmatrix, const int& elementnumber, const int& feorder, int& modenumber,
  std::vector<int>& k0, std::vector<int>& k1, VectorType& xk, VectorType& yk,
  double& pi)
{
  const int n = febasis.size();
  stiffnessmatrix.setSize(n,n);
  massmatrix.setSize(n,n);

  // Get the grid view from the finite element basis
  typedef typename FEBasis::GridView GridView;
  GridView gridView = febasis.gridView();

  // MatrixIndexSets store the occupation pattern of a sparse matrix.
  // They are not particularly efficient, but simple to use.
  MatrixIndexSet occupationPattern;
  getOccupationPattern(febasis, occupationPattern, elementnumber, feorder);

  // ... and give it the occupation pattern we want.
  occupationPattern.exportIdx(stiffnessmatrix);
  occupationPattern.exportIdx(massmatrix);

  // Set all entries to zero
  stiffnessmatrix = 0;
  massmatrix = 0;

  // A view on the FE basis on a single element
  auto localView = febasis.localView();

  // A loop over all elements of the grid
  for(const auto& e : elements(gridView))
  {

    // Bind the local FE basis view to the current element
    localView.bind(e);

    // Now let's get the element stiffness matrix
    // A dense matrix is used for the element stiffness matrix
    Matrix<FieldMatrix<double,1,1>> localStiffnessMatrix;
    // getLocalStiffnessMatrix(localView, localstiffnessmatrix, modenumber, k0, k1, xk, yk);
    Matrix<FieldMatrix<double,1,1>> localMassMatrix;
    // getLocalMassMatrix(localView, localmassmatrix, modenumber, k0, k1, xk, yk);
    getLocalMatrices(localView, localStiffnessMatrix, localMassMatrix, modenumber, k0, k1, xk, yk, pi);

    // Add element stiffness matrix onto the global stiffness matrix
    for (size_t i=0; i<localStiffnessMatrix.N(); i++) {

      // The global index of the i-th local degree of freedom of the element 'e'
      auto row = localView.index(i);
      indexCorrection(row[0], elementnumber, feorder);

      for (size_t j=0; j<localStiffnessMatrix.M(); j++ ) {

        // The global index of the j-th local degree of freedom of the element 'e'
        auto col = localView.index(j);
        indexCorrection(col[0], elementnumber, feorder);

        stiffnessmatrix[row][col] += localStiffnessMatrix[i][j];
        massmatrix[row][col] += localMassMatrix[i][j];

      }

    }

  }

  // Make the stiffness matrix to the identity for all boundary nodes to ensure that the final matrix is invertible
  for(int i = 0; i < n; i++){
    if (isOnBoundary(i, elementnumber, feorder)){
      stiffnessmatrix[i][i] = 1;
    }
  }

}


// Copy all values on the bottom/left to the identified boundary points on the top/right
template <class VectorType>
void copyToBoundary(VectorType& vector, const int& n, const int& elementnumber,
  const int& feorder)
{
  for(int i = 0; i < n; i++){
    if (isOnBoundary(i, elementnumber, feorder)){
      auto j = i;
      indexCorrection(j, elementnumber, feorder);
      vector[i] = vector[j];
    }
  }
}


// Interpolate the surface function on a given grid view
template <class FEBasis, class GridView, class VectorType>
void interpolateSurfaceFunction(const FEBasis& febasis, GridView& gridview,
  VectorType& surface, std::vector<char>& nonboundarynodes, const int& elementnumber,
  const int& feorder, int& modenumber, std::vector<int>& k0, std::vector<int>& k1,
  VectorType& xk, VectorType& yk, double& pi)
{
  const int n = febasis.size();

  auto graphFunction  = [modenumber, k0, k1, xk, yk, pi](const auto x) {
    double value = 0;
    double sp;

    for(int i=0; i < modenumber; i++){
      sp = 2*pi*(x[0]*k0[i] + x[1]*k1[i]);
      value += std::sin(sp)*xk[i] - std::cos(sp)*yk[i];
    }
    value /= pi;
    return value;
  };

  surface.resize(n);

  interpolate(febasis, surface, graphFunction, nonboundarynodes);
  copyToBoundary(surface, n, elementnumber, feorder);
}


// Compute one implicit Euler step
template <class FEBasis, class Solver, class VectorType, class MatrixType>
void implicitEuler(const FEBasis& febasis, Solver& solver, VectorType& iter,
  MatrixType& massmatrix)
{
  const int n = febasis.size();

  VectorType rhs;
  rhs.resize(n);
  rhs = 0;

  massmatrix.mv(iter,rhs);

  iter = 0;

  InverseOperatorResult statistics;
  solver.apply(iter, rhs, statistics);
}


int main (int argc, char *argv[]) try
{
  MPIHelper::instance(argc, argv);


  // Initialize stuff
  std::string name1;
  std::string name2;
  name1 = "_" + std::to_string(eta) + "_" + std::to_string(kappa) + "_" + std::to_string(sigma);
  name2 = name1 + "_" + std::to_string(elementNumber) + "_" + std::to_string(feOrder);

  typedef BlockVector<FieldVector<double,1>> VectorType;
  typedef BCRSMatrix<FieldMatrix<double,1,1>> MatrixType;

  typedef std::conditional<solver==0, Cholmod<VectorType>, UMFPack<MatrixType>>::type SolverType;

  double pi = std::acos(-1.0);

  Timer timer;


  // Setting up the random surface
  int modeNumber;
  std::vector<int> k0;
  std::vector<int> k1;

  indexSet(eta, modeNumber, k0, k1);

  std::cout << "Number of Fourier modes is " << 2*modeNumber << std::endl;

  VectorType Xk;
  VectorType Yk;

  if (answer1 == "y"){
    generateSurfaceFunction(modeNumber, k0, k1, Xk, Yk, kappa, sigma);

    if (answer2 == "y"){
      storeMatrixMarket(Xk, "X" + name1);
      storeMatrixMarket(Yk, "Y" + name1);
    }
  }
  else{
    loadMatrixMarket(Xk, "X" + name1);
    loadMatrixMarket(Yk, "Y" + name1);
  }


  // Generation of the grid
  Dune::YaspGrid<2> grid({1,1},{elementNumber,elementNumber});

  auto gridView = grid.leafGridView();

  using GridView = decltype(gridView);


  // Assembling of the matrices
  typedef Functions::LagrangeBasis<GridView,feOrder> FEBasis;
  FEBasis feBasis(gridView);

  auto localView = feBasis.localView();

  const int n = feBasis.size();
  std::cout << "Number of DOFs is " << n << std::endl;

  MatrixType stiffnessMatrix;
  MatrixType massMatrix;

  if (answer3 == "y"){
    timer.reset();
    assembleMatrices(feBasis, stiffnessMatrix, massMatrix, elementNumber, feOrder, modeNumber, k0, k1, Xk, Yk, pi);
    std::cout << "Assembling the matrices took " << timer.elapsed() << "s" << std::endl;

    if (answer4 == "y"){
      storeMatrixMarket(stiffnessMatrix,"stiffnessmatrix" + name2);
      storeMatrixMarket(massMatrix,"massmatrix" + name2);
    }
  }
  else{
    loadMatrixMarket(stiffnessMatrix,"stiffnessmatrix" + name2);
    loadMatrixMarket(massMatrix,"massmatrix" + name2);
  }

  auto& operatorMatrix = stiffnessMatrix;
  operatorMatrix *= stepSize;
  operatorMatrix += massMatrix;


  // Save the nonboundary nodes
  std::vector<char> nonBoundaryNodes;
  nonBoundaryNodes.resize(n);
  for(int i = 0; i < n; i++){
    if (isOnBoundary(i, elementNumber, feOrder) == false){
      nonBoundaryNodes[i] = true;
    }
  }


  // Generating the surface data
  VectorType surface;
  if (answer5 == "y"){
    timer.reset();
    interpolateSurfaceFunction(feBasis, gridView, surface, nonBoundaryNodes, elementNumber, feOrder, modeNumber, k0, k1, Xk, Yk, pi);
    std::cout << "Interpolating the surface function took " << timer.elapsed() << "s" << std::endl;
  }


  // Initial value handling
  VectorType iter;
  iter.resize(n);

  auto initialValueFunction  = [pi](const auto x) { return 10000/pi*std::exp(-10000*((x[0]-0.5)*(x[0]-0.5) + (x[1]-0.5)*(x[1]-0.5))); };
  interpolate(feBasis, iter, initialValueFunction, nonBoundaryNodes);
  copyToBoundary(iter, n, elementNumber, feOrder);


  // Generating the output files
  auto iterFunction = Functions::makeDiscreteGlobalBasisFunction<double>(feBasis, iter);
  auto surfaceFunction = Functions::makeDiscreteGlobalBasisFunction<double>(feBasis, surface);

  auto vtkWriter = std::make_shared<SubsamplingVTKWriter<GridView>>(gridView, refinementLevels(refinementNumber));
  VTKSequenceWriter<GridView> vtkSequenceWriter(vtkWriter, "solution");
  vtkWriter->addVertexData(iterFunction, VTK::FieldInfo("solution", VTK::FieldInfo::Type::scalar, 1));
  if (answer5 == "y"){
    vtkWriter->addVertexData(surfaceFunction, VTK::FieldInfo("surface", VTK::FieldInfo::Type::scalar, 1));
  }
  vtkSequenceWriter.write(0.0);


  // Initialize time integration
  double time = 0;


  // Setting up the solver
  timer.reset();
  SolverType directSolver;
  directSolver.setMatrix(operatorMatrix);
  std::cout << "Solving the linear system took " << timer.elapsed() << "s" << std::endl;


  // Counter to export logarithmic time-steps
  int logExport = 1;


  // Time integration
  for (int i = 1; i <= stepNumber; i++){

    time += stepSize;

    timer.reset();
    implicitEuler(feBasis, directSolver, iter, massMatrix);
    std::cout << timer.elapsed() << "s" << std::endl;

    if (skipExport < 0){
      if (i == logExport){
        copyToBoundary(iter, n, elementNumber, feOrder);
        vtkSequenceWriter.write(time);
        logExport *= abs(skipExport);
      }
    }
    else {
      if (i % skipExport == 0){
        copyToBoundary(iter, n, elementNumber, feOrder);
        vtkSequenceWriter.write(time);
      }
    }

  }


  return 0;
}


// Error handling
catch (Exception& e) {
  std::cout << e.what() << std::endl;
}
