#include <string>

// Number of elements per square side
const int elementNumber = 100;

// Order of the FEM
const int feOrder = 1;

// Maximal norm of Fourier nodes
const int eta = 10;

// Material constants
const double kappa = 0;
const double sigma = 1;

// Number of grid refinements
const int refinementNumber = 1;

// Number of steps of the implicict Euler scheme
const int stepNumber = 100;

// Stepsize of the implicict Euler scheme
const double stepSize = 0.0005;

// Positive integer n means that the steps 1*n, 2*n, 3*n, ... are getting exported (equidistant)
// Negative integer -n means that the steps n^0, n^1, n^2, ... are getting exported (logarithmic)
const int skipExport = -10;

// Do you want to generate a new surface function? (y/n)
const std::string answer1 = "y";

// Do you want to save the current surface function? (y/n)
const std::string answer2 = "n";

// Do you want to generate new matrices? (y/n)
const std::string answer3 = "y";

// Do you want to save the current matrices? (y/n)
const std::string answer4 = "n";

// Do you want to export the surface? (y/n)
const std::string answer5 = "y";

// Direct solver to be used (0=Cholmod, 1=UMFPack)
const int solver = 0;
