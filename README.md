# dune-simon

## Scope of the module

This module was developed as part of my Master's thesis. Its goal is to solve the heat equation 
on a randomly generated surface coming from the Helfrich membrane model.


## Documentation

The simulation's parameters can be specified in a header file, called `options.h`, which is located in the build directory after the building process is finished. It is of the following format where some default parameters are already defined to let it compile successfully during the building process.

```cpp
#include <string>

// Number of elements per square side
const int elementNumber = 100;

// Order of the FEM
const int feOrder = 1;

// Maximal norm of Fourier nodes
const int eta = 10;

// Material constants
const double kappa = 0;
const double sigma = 1;

// Number of grid refinements
const int refinementNumber = 1;

// Number of steps of the implicict Euler scheme
const int stepNumber = 100;

// Stepsize of the implicict Euler scheme
const double stepSize = 0.0005;

// Positive integer n means that the steps 1*n, 2*n, 3*n, ... are getting exported (equidistant)
// Negative integer -n means that the steps n^0, n^1, n^2, ... are getting exported (logarithmic)
const int skipExport = -10;

// Do you want to generate a new surface function? (y/n)
const std::string answer1 = "y";

// Do you want to save the current surface function? (y/n)
const std::string answer2 = "n";

// Do you want to generate new matrices? (y/n)
const std::string answer3 = "y";

// Do you want to save the current matrices? (y/n)
const std::string answer4 = "n";

// Do you want to export the surface? (y/n)
const std::string answer5 = "y";

// Direct solver to be used (0=Cholmod, 1=UMFPack)
const int solver = 0;
```

As one can see the user gets the option to save and/or load matrices and surfaces. The generated 
files follow a strict naming based on the input parameters and should hence not be renamed. At 
the moment it is not possible to save more than one surface or matrix for the same configuration 
of parameters, by doing so the old files get overwritten.

The last option gives the choice of including the considered surface into the output files.


## Building dune-simon

### Dependencies
Dune-simon depends on the [dune-core][core] modules, [dune-typetree][typetree],
[dune-uggrid][uggrid] and [dune-functions][functions]. All of them are available via git:

* https://gitlab.dune-project.org/core/dune-common
* https://gitlab.dune-project.org/core/dune-geometry
* https://gitlab.dune-project.org/core/dune-grid
* https://gitlab.dune-project.org/core/dune-istl
* https://gitlab.dune-project.org/core/dune-localfunctions
* https://gitlab.dune-project.org/staging/dune-typetree
* https://gitlab.dune-project.org/staging/dune-uggrid
* https://gitlab.dune-project.org/staging/dune-functions

Furthermore the `Suitesparse` library has to be installed on the executing system.

### Building the module
Dune-simon integrates into the cmake-based dune build system.
Hence it can be build (like any other module) using the `dunecontrol` script provided by the 
core  modules. For details on how to use this build system and how to specify build options have 
a look at the documentation in the dune-common module.


[core]: https://dune-project.org/groups/core
[typetree]: https://gitlab.dune-project.org/staging/dune-typetree
[uggrid]: https://gitlab.dune-project.org/staging/dune-uggrid
[functions]: https://gitlab.dune-project.org/staging/dune-functions
